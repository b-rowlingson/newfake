
NewFake
=======

Make new fake places. 

```
nc = st_read(system.file("shape/nc.shp", package="sf"))
nc_outline = summarize(group_by(nc))

districts = make_regions(nc_outline, 200)
county_structure = fakemerged(districts, 10)

districts$county = county_structure$membership
counties = county_structure$borders

plot(nc_outline)
plot(counties$geometry, add=TRUE, lwd=2)
plot(districts$geometry, add=TRUE, lty=2)
 

```
